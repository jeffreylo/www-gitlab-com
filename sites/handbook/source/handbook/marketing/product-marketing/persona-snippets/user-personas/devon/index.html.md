---
layout: handbook-page-toc
title: "Devon Persona snippets"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## SDR persona snippets by use case

### [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

**Overview**
- description

#### [VC&C use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [CI use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [CD use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/cd/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [DevSecOps use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/devsecops/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...
