---
layout: markdown_page
title: Product Direction - Product Analytics
description: "Product Analytics manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/product-analytics/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

The vision of Product Analytics is to help us build a better Gitlab. We collect data about how GitLab is used to understand what product features to build, to understand how to serve our customers, and to help our customers understand their DevOps lifecycle.

## Roadmap

### Privacy Policy Rollout

Steps: Privacy Policy, Privacy Settings

Teams Involved: Legal, Product Analytics, Data

**Current State**
- Our [privacy policy](https://about.gitlab.com/privacy/) has not been updated since the [October 29, 2019 Update on free software and telemetry](https://about.gitlab.com/blog/2019/10/10/update-free-software-and-telemetry/). This policy needs to be updated to align with our Product Analytics direction.
- Privacy settings in GitLab are currently scattered in several places.

**Roadmap**
- [Rollout an updated Privacy Policy](https://gitlab.com/groups/gitlab-com/-/epics/907)
- Build a centralized privacy settings page in GitLab for users to easily understand and control their privacy settings.

### Build Our Collection Framework

Steps: Collection Framework, Event Dictionary, Instrumentation

Teams Involved: Product Managers, Product Analytics

**Current State:**

![](collection_framework_fy21_q4_current.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

- Client and server-side counts can now be tracked on SaaS and Self-Managed.
- Client and server-side events are a work in progress on Self-Managed.
- Plan-level reporting using SaaS Usage Ping is not possible as SaaS is multi-tenant and Usage Ping reports at an Instance level.
- We've implemented an [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)
- Currently, for our SMAU / Section MAU metrics, we use the highest component in each grouping. For example, for SMAU, we take the highest GMAU in the group. This methodology of calculating SMAU / Section MAU is lacking as uses one metric to represent the whole group instead of finding the union across the entire group.

**Roadmap:**

![](collection_framework_fy21_q4_future.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

1. Q3: Plan-level reporting for Usage Ping Redis on SaaS for multi-tenant reporting
1. Q3: Plan-level and Group-level reporting for Usage Ping Postgres on SaaS for multi-tenant reporting
1. Q4: Usage Ping Snowplow on Self-Managed (using internal collector and database for moderate volume)
1. Q4: Group-level, Project-level, User-level tracking for Snowplow on SaaS
1. FY22-Q1: Usage Ping Snowplow on SaaS (using external collector and database for scaled up volume)

- Q3: Create [de-duplicated GMAU and SMAU metrics](https://gitlab.com/gitlab-org/product-analytics/-/issues/421) in Usage Ping before the Usage Ping is sent. This will help us achieve more accurate XMAU metrics.
- Q4: Restructure Usage Ping with nesting hierarchy and naming convention (time period, segmentation, event name)
- We need to implement an Event Dictionary for Snowplow

### Decrease Analytics Cycle Times

Steps: Release Cycle, Events Fired, Usage Ping Generated, Analytics Collectors, Data Imports, Data Warehouse, Data Modelling

Teams Involved: Product Analytics, Data

**Current State:**

- Data Availability cycle times are currently a 51 day cycle
- Exports of the Versions DB are currently done manually every 14 days according to [this schedule](https://gitlab.com/groups/gitlab-data/-/epics/162).

**Roadmap:**

- [Automate Versions DB exports](https://gitlab.com/gitlab-org/product-analytics/-/issues/398) to reduce cycle times by 14 days.

### Plan and Group-level Reporting for SaaS

Steps: Collection Framework, Data Availability, Data Warehouse, Data Modelling

Teams Involved: Product Analytics, Data 

**Current State**

- Currently Usage Ping is not segmented by plan type which means for any SaaS free / paid segmentation cannot be done using Usage Ping. Instead, as a work around, the data team is using the Postgres database import and manually recreating all Usage Ping queries in Sisense.

**Roadmap**
- [Enable Plan-level reporting of SaaS Usage Ping](https://gitlab.com/gitlab-org/product-analytics/-/issues/423). This will allow Usage Ping to be separated by free and paid on SaaS so that the data team doesn’t need to manually recreate all Usage Ping queries in Sisense.
- We need to shift the entire usage ping workload into the data warehouse. We already struggle to generate a single usage ping in the production database, we cannot 1Mx this to run it at the group level. 

### Implement Product Performance Indicators

Steps: Instrumentation, Sisense Dashboard, Performance Indicators, PI Target, PI Reviews, Improve Product

Teams Involved: Product Managers, Product Analytics, Data

**Current State**

- Currently not all section, stages, and groups have performance indiciators in place.
- Currently Sisense dashboards are all custom made with little standardization.
- Targets are currently set manually based off little data.
- PI Reviews are done using slide decks 

**Roadmap** 

- Lead the [Product Org FY21-Q3 PI OKRs](https://gitlab.com/gitlab-com/Product/-/issues/1320). Progress can be tracked in [Track PI OKR Status](https://gitlab.com/groups/gitlab-com/-/epics/959)
  - KR1 (EVP, Product): [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
  - KR2 (EVP, Product): [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343)
- To support this work, we need to guide and review each product group’s tracking code. In some cases, we will need to directly do the implementation work.
- Create a standardized dashboard template that can be reused across all PIs.
- Create process to set PI targets.
- Allow targets to be set dynamically.
- Move towards using the Section PI pages and [Stage and Group PI Page](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) to conduct monthly reviews.
- Create Monthly PI review process.

### Enable Sales and Customer Success

Steps: Salesforce Data Feed, Salesforce Dashboard, Gainsight Dashboard, Customer Conversations

Teams Involved: Sales, Customer Success, Product Analytics, Data

**Current State**
- Support the [FY21-Q3 Deploying Product Analytics for CRO Org](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#). For more information, see [CRO Product Analytics: Status, Gaps and the Road Forward](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#).
- We've [Replicate Versions App Reporting in Sisense Dashboards](https://gitlab.com/gitlab-data/analytics/-/issues/4488/)
- License data has been added to this data feed

**Roadmap**

- Usage Ping data is in the process of being added to this data feed
- [Customer Adoption Journey](https://gitlab.com/groups/gitlab-org/-/epics/3572)


### Make Collected Data Useful For Customers

Steps: Value Stream Data, Value Stream Features

Teams: Manage, Product Analytics

**Current Status:**

- WIP

**Roadmap:**

- WIP

### Enable Full Funnel Analytics 

Steps: Marketing Data, Product Data, Fulfillment Data, Sales Data, Customer Success Data, Full Funnel Analysis

Teams: Marketing, Fulfillment, Sales, Customer Success, Product Analytics, Data, Enterprise Applications

**Current Status:**

- WIP

**Roadmap:**

- WIP

# Product Analytics Overview

To accomplish our vision, we use the product analytics process which outlines all of the steps involved to go from collecting data to making it useful for our internal teams and customers.

1) Collect Data Transparently

```mermaid
graph LR
	privacy_policy[Privacy Policy] --> privacy_settings[Privacy Settings]
	privacy_settings[Privacy Settings] --> collection_framework[Collect Framework]
	collection_framework[Collect Framework] --> event_dictionary[Event Dictionary]
	event_dictionary[Event Dictionary] --> instrumentation[Instrumentation]
	instrumentation[Instrumentation] --> release_cycle[Release Cycle]
	release_cycle[Release Cycle] --> events_fired[Events Fired]
	events_fired[Events Fired] --> usage_ping_generated[Usage Ping Generated]
```

2) Processing Pipeline

```mermaid
graph LR
    analytics_collectors[Analytics Collectors] --> data_imports[Data Imports]
	data_imports[Data Imports] --> data_warehouse[Data Warehouse]
	data_warehouse[Data Warehouse] --> data_modelling[Data Modelling]
```

3) Enable Product Teams

```mermaid
graph LR
	sisense_dashboard[Sisense Dashboard] --> performance_indicators[Performance Indicators]
	performance_indicators[Performance Indicators] --> pi_target[PI Target]
	pi_target[PI Target] --> pi_reviews[PI Reviews]
	pi_reviews[PI Reviews] --> improve_product[Improve Product]
```

4) Enable Sales and Customer Success Teams

```mermaid
graph LR
	salesforce_data_feed[Salesforce Data Feed] --> salesforce_dashboard[Salesforce Dashboard]
	salesforce_dashboard[Salesforce Dashboard] --> gainsight_dashboard[Gainsight Dashboard]
	gainsight_dashboard[Gainsight Dashboard] --> customer_conversations[Customer Conversations]
```

5) Make Collected Data Useful For Customers

```mermaid
graph LR
	value_stream_data[Value Stream Data] --> value_stream_features[Value Stream Features]
```

6) Enable Full Funnel Analytics

```mermaid
graph LR
	marketing_data[Marketing Data] --> product_data[Product Data]
	product_data[Product Data] --> fulfillment_data[Fulfillment Data]
	fulfillment_data[Fulfillment Data] --> sales_data[Sales Data]
	sales_data[Sales Data] --> customer_success_data[Customer Success Data]
	customer_success_data[Customer Success Data] --> full_funnel_analysis[Full Funnel Analysis]
```

**Teams Involved**

We work closely with other internal teams in each step in the Product Analytics Process. The teams involved at each step are:

| Step                 | Teams Involved                      |
|----------------------|-------------------------------------|
| Privacy Policy       | Legal, Product Analytics, Data      |
| Privacy Settings     | Legal, Product Analytics, Data      |
| Collection Framework | Product Analytics                   |
| Event Dictionary     | Product Managers, Product Analytics |
| Instrumentation      | Product Managers, Product Analytics |
| Release Cycle        | Product Managers, Product Analytics |
| Events Fired         | Product Managers, Product Analytics |
| Usage Ping Generated | Product Managers, Product Analytics |
| Analytics Collectors | Product Analytics, Data             |
| Data Imports         | Product Analytics, Data             |
| Data Warehouse       | Product Analytics, Data             |
| Data Modelling       | Product Analytics, Data             |
| Sisense Dashboard      | Product Managers, Product Analytics, Data |
| Performance Indicators | Product Managers, Product Analytics, Data |
| PI Target              | Product Managers, Product Analytics, Data |
| PI Reviews             | Product Managers, Product Analytics, Data |
| Improve Product        | Product Managers                          |
| Salesforce Data Feed   | Sales, Customer Success, Product Analytics, Data |
| Salesforce Dashboard   | Sales, Customer Success, Product Analytics, Data |
| Gainsight Dashboard    | Sales, Customer Success, Product Analytics, Data |
| Customer Conversations | Sales, Customer Success                          |
| Value Stream Data      | Manage, Product Analytics |
| Value Stream Features  | Manage, Product Analytics |
| Marketing Data         | Marketing, Product Analytics, Data, Enterprise Applications          |
| Product Data           | Product Analytics, Data, Enterprise Applications                     |
| Fulfillment Data       | Fulfillment, Product Analytics, Data, Enterprise Applications        |
| Sales Data             | Sales, Product Analytics, Data, Enterprise Applications              |
| Customer Success Data  | Customer Success, Product Analytics, Data, Enterprise Applications   |
| Full Funnel Analysis   | Marketing, Fulfillment, Sales, Customer Success, Product Analytics, Data, Enterprise Applications   |

# Product Metrics Guide

We track our product performance via our [Product Performance Indicator pages](/handbook/product/performance-indicators/#other-pi-pages). We are constantly on a journey to improve our Product Metrics and mature how we utilize them. That process generally follows the following steps:

1. Determine the level at which the metric should be measured:
    1. All product level - TMAU, Paid TMAU
    1. Stage level - SMAU, Paid SMAU, Other PI
    1. Group level - GMAU, Paid GMAU, Other PI
1. Define the metric. This is typically done by selecting an initial AMAU that is most representative of overall stage/group use
1. Determine the right [collection framework](/direction/product-analytics/#collection-framework) to utilize. Some guidance for considering collection frameworks:
  * **Usage Ping - General** - There are many frameworks that report via Usage Ping. All of them have various capabilities regarding types of events and available segementation. All Usage Ping collection frameworks have a frequency of monthly for self-managed usage and every-other-week for SaaS usage. 
   * **Snowplow** - Investment in instrumentation should be discouraged - there is already great URL and path tracking from SnowPlow for Gitlab.com. If additional tracking is needed on front-end events consider utilizizing Usage Ping Redis HLL. Snowplow instrumentation becomes immediately available on GitLab.com only.
   * **Usage Ping - Redis HLL** - Redis HLL can track distinct counts, typically in front-end metrics on both the client (Javasciprt) and server (Ruby) side. Redis HLL does not provide session level segmentationon GitLab.com. As Usage Ping instrumentation it is returned from both SaaS and Self-Managed. 
   * **Usage Ping - Postgres** - Our original Usage Ping implementation this collects distinct database counts. 
   * **Usage Ping - Prometheus** - Note there is no future plan to increase the segmentation of this collection framework and it is presently limited to Instance wide metrics.
1. Instrument the metric. This is typically done by leveraging usage ping to track unique user counts. There are a number of technical options outlined here.
1. Dashboard the metric. This is done by creating a Sisense dashboard. Avoid cumulative views and instead focus on month-over-month growth. Instructions for creating dashboards are here.
1. Set a growth target, and embed in the tracking dashboard. Growth targets should be ambitious but achievable.
1. Deeply understand the drivers of your metric through customer interviews and other product usage data. Visualize this through a user adoption funnel that describes how users enjoy the value of the particular product area you are measuring.
1. Line up your roadmap priorities to positively influence the adoption funnel, drive the performance metric, and hit the growth targets.
1. Continually monitor your results, and refine your priorities based on results.

## Product Analytics Workflow

The Product Analytics workflow was presented in the Weekly Product Meeting on August 11, 2020. For a recap of this, please see the [slide deck](https://docs.google.com/presentation/d/1wCpvdCUtBtU4Y1vHDOSOLjhrlPvdYBAbydj58SRN5Js/edit) and [watch the video presentation](https://youtu.be/1l1ru7k-q2I?t=375).

### Objective

The objective of the Product Analytics workflow is to help the product organization achieve the Product Organizations FY21-Q3 OKRs:

- [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343).

To achieve this, we will be taking the following approach:

1. Get a XMAU in place for your section, stage, or group ASAP using [currently available tooling](https://docs.gitlab.com/ee/development/product-analytics/).
1. If blocked, meet with [@jeromezng](https://gitlab.com/jeromezng) [@kathleentam](https://gitlab.com/kathleentam) to find work around solutions for your XMAU.
1. Improve your XMAU in future iterations as the collection framework improves.

To track the stataus of this OKR, we'll temporarily be using the [XMAU Tracking Sheet](https://docs.google.com/spreadsheets/d/18er-ZpqdtZDhs_bmZvJaU_iVsa3R38VMiau4C6RoPPs/edit#gid=706608817). This sheet is a temporary measure and we have an [epic](https://gitlab.com/groups/gitlab-com/-/epics/906) that's currently in progress to move this into the Product PI handbook page.

Note: XMAU is used to represent all the variations of AMAU, GMAU, Paid GMAU, SMAU, Paid SMAU, Section MAU, and Paid Section MAU.

### Implementation Status

The Product Analytics workflow outlines the steps required for putting a product performance indicator in place.

| Status | Description | Responsibility |
| ------ | ----------- | -------------- |
| [Privacy Policy](#privacy-policy) | The privacy policy step describes what product usage data we can collect from our users. | Product Analytics Responsibility |
| [Collection Framework](#collection-framework) | The collection framework step describes our available tooling for collecting product usage data. | Product Analytics Responsibility |
| [Event Dictionary](#event-dictionary) | The event dictionary step outlines our single source of truth of the product metrics and events we collect. | PM Responsibility, Product Analytics Support |
| [Instrumentation](#instrumentation) | The instrumentation step outlines how each product team implements data collection. | PM Responsibility, Product Analytics Support |
| [Data Availability](#data-availability) | The data availability step outlines the timing of a product release to receiving product usage data in the data warehouse. | PM Responsibility, Product Analytics Support |
| [Data Warehouse](#data-warehouse) | The data warehouse step outlines where our product usage data is stored. | Product Analytics Responsibility |
| [Data Model](#data-model) | The data modelling step outlines how we transform raw data into a data structure that's ready for analysis. | Product Analytics Responsibility |
| [Dashboard](#dashboard) | The dashboarding step outlines how Sisense dashboards are built. | PM Responsibility, Product Analytics Support |
| [Handbook](#handbook) | The handbook PI page describes how product performance indicators are added for each product section, stage, and group. | PM Responsibility, Product Analytics Support |
| [Target](#target) | The target definition step outlines how targets are defined for each performance indicator. | PM Responsibility, Product Analytics Support |
| [Complete](#complete) | All of the prior steps have been completed. | PM Responsibility, Product Analytics Support |

### Next Steps

1. @jeromezng @kathleentam will meet with all stages.
1. Add metrics and definitions into the Event Dictionary.
1. Instrument tracking by focusing on XMAU candidates.
1. Wait for data availability.
1. Create standardized dashboards.
1. Update handbook PI pages with XMAU definitions.
1. Add targets to each PI.

### Privacy Policy

For our current state, see: [Privacy Policy](https://about.gitlab.com/privacy/). For our roadmap, see: [Rollout Plan: Privacy Policy for Product Usage Data](https://gitlab.com/groups/gitlab-com/-/epics/907).

### Collection Framework

For our current state and roadmap, see: [Product Analytics Direction Page: Collection Framework](https://about.gitlab.com/direction/product-analytics/#build-collection-framework).

### Event Dictionary

We currently have 500+ existing usage ping metrics but no proper documentation of what actions they track. The Event Dictionary will allow us to create a single source of truth for the metrics and events we collect. It will also help PMs better understand what data is currently collected and what metrics can potential be used as an XMAU. The event dictionary includes:

- 500+ Usage Ping metrics
- Section, stage, or group assigned to the metric
- Metric description
- Implementation status
- Availability by plan type
- Code path of metric

Note: we've temporarily moved the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485) from our Markdown docs into a sheet for easier editing. We'll eventually move this [back into our docs](https://docs.gitlab.com/ee/development/product_analytics/event_dictionary) once the majority of changes are done.

**Instructions:**

1. Complete the [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174) issue for your section: [Dev](https://gitlab.com/gitlab-org/gitlab/-/issues/234301), [Ops](https://gitlab.com/gitlab-org/gitlab/-/issues/234577), [Enablement](https://gitlab.com/gitlab-org/gitlab/-/issues/234576), [Secure & Defend](https://gitlab.com/gitlab-org/gitlab/-/issues/234578), [Growth](https://gitlab.com/gitlab-org/gitlab/-/issues/234580).
1. Open the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485) and fill in “PM to edit” columns.
1. Plan your future events and metrics with your engineering team and add them to the Event Dictionary.

### Instrumentation

Work with your engineering team to instrument tracking for your XMAU. Focus on using Usage Ping as your metrics will be available on both SaaS and self-managed.

The [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html) outlines the steps required for instrumentation. It includes:

- [What is Usage Ping?](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#what-is-usage-ping)
- [How Usage Ping works](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#how-usage-ping-works)
- [Implementing Usage Ping](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#implementing-usage-ping)
- [Developing and Testing Usage Ping](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#developing-and-testing-usage-ping)
- [Example Payload](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#example-usage-ping-payload)

Also see the [Product Analytics Guide](https://docs.gitlab.com/ee/development/product_analytics/) and [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html).

**Instructions:**

1. Read the docs and work with your engineers on instrumentation.
1. Ask for a [Product Analytics Review](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#8-ask-for-a-product-analytics-review) in your MR.

### Data Availability

Plan instrumentation with sufficient lead time for data availability. Ensure your metrics make it into the self-managed release as early as possible.

**Timeline:**

1. [Self-managed releases](https://about.gitlab.com/upcoming-releases/) happen on the 22nd of each month (+30 days)
1. Wait one week for customers to upgrade to the new release and for a Usage Ping to be generated (+7 days)
1. Usage Pings are collected in the Versions application. The Versions database is imported into the Snowflake Data Warehouse manually every two weeks (+14 days). Note that we’re working to [automate this to daily imports](https://gitlab.com/gitlab-org/product-analytics/-/issues/398) (bringing this down to +1 day instead).

In total, plan for up to 51 day cycle times (Examples [1](https://gitlab.com/gitlab-data/analytics/-/issues/5629#note_389671640), [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31785#note_392882428)). Cycle times are slow with monthly releases and weekly pings, so, implement your metrics early.

**Instructions:**

1. Merge your metrics into the next self-managed release as early as possible.
1. Wait for your metrics to be released onto production GitLab.com. These releases currently happen on a daily basis.
1. Usage Pings are generated on GitLab.com on a weekly basis. Monitor the #g_product_analytics slack channel where the Product Analytics team will post the latest GitLab.com Usage Ping ([example](https://gitlab.slack.com/files/ULXG09FAL/F01905UPPL0/12-gitlab.com-usage-data-for2020-08-04.json?origin_team=T02592416&origin_channel=CL3A7GFPF)). Verify your new metrics are present in the GitLab.com Usage Ping payload.
1. Wait for the Versions database to be imported into the data warehouse.
1. Check the dbt model [version_usage_data_unpacked](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_unpacked#columns) to ensure the database column for your metric is present.
1. Check [Sisense](http://app.periscopedata.com/app/gitlab/) to ensure data is available in the data warehouse.

### Data Warehouse

See [Data Team Platform: Data Warehouse](https://about.gitlab.com/handbook/business-ops/data-team/platform/#data-warehouse)

### Data Model

See [Data Team dbt Guide](https://about.gitlab.com/handbook/business-ops/data-team/platform/dbt-guide/)

### Dashboard

We need PMs to self-serve their own dashboards as data team capacity is limited. The data team will be focused on enabling self-service, advising PMs, and working on the more challenging XMAU dashboards.

To learn how to create your own dashboard, see [Data For Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)

To update the SMAU Summary Dashboards: [GitLab.com Postgres SMAU Dashboard (SaaS)](https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU) and [Usage Ping SMAU Dashboard (SaaS + Self-Managed)](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard), please open a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues).

**Dashboard Prioritization**

For GMAU and SMAU data issues:

- In Q3, due to very limited data team capacity, the data team capacity will be reserved primarily for GMAU and SMAU.
    - Please add `XMAU` label to the data issues.
    - Please limit the ask to XMAU only, rather than all the supporting metrics.
- If there is still need to prioritize within GMAU issues, we will work with Scott and Section Leaders on the prioritization.
    - Section Leaders are encourage to [rank XMAU Issues](https://gitlab.com/gitlab-data/analytics/-/issues/5664#note_392529098) on the [Product Section Board](https://gitlab.com/gitlab-data/analytics/-/boards/1921369?label_name%5B%5D=Product).

For non-GMAU and non-SMAU data issues:

- In Q3, the data team will have very limited capacity to support non-GMAU and non-SMAU data requests.
    - Please continue to create data issues and label Data issues with the appropriate [Product Section Label](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=section).
- If there are critical or urgent data asks, please @hilaqu and your section leader in the issue, with an explanation of why. We will evaluate them on a case-by-case basis.

**Instructions:**

1. Read through [Data For Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)
1. Self-serve your dashboard
1. If you need help, create a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues) and ask your Section Leader to prioritize.

### Handbook

There are five Product PI pages: The [Product Team page](https://about.gitlab.com/handbook/product/performance-indicators/) and section pages for [Dev](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/), [Ops](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/), [Secure & Protect](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/), [Enablement](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/).

We need all PMs to ensure their PIs are showing on the performance indicator pages. Based off [What we're aiming for](#what-were-aiming-for)

To do so, we need a clear way to communicate to PMs exactly which PIs are remaining. We will be [adding placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906) into the [performance indicator file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) so that all required PIs show in the handbook. Once a PI is implemented, the actual PI will replace the placeholder PI.For more information about how PIs and XMAUs are related to one another, see [PI Structure](https://about.gitlab.com/handbook/product/performance-indicators/#structure).

**Instructions:**

1. @jeromezng @kathleentam will [add placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906).
1. @jeromezng @kathleentam will then meet with each Section, Stage, and Group, to understand the implementation status of each PI and document any exceptions. Exceptions for PIs will then be signed off by Scott, Anoop, and the Section Leaders.
1. Keep your [performance indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) up to date as the implementation status changes.

### Target

As a product organization, we need to get into the habit of understanding our baselines and setting targets for each stage & group. For the PI Target step, you will work with your Section or Group Leader to define targets for each of your XMAUs.

**Instructions:**

1. Work with your Section or Group Leader to define a target.
1. Add the Target Line into your dashboard ([example](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8490496)).
1. If you need help, book a meeting with @jeromezng @kathleentam.

### Complete

All of the prior steps have been completed and a PI is successfully implemented.

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Analytics Product Direction](/direction/product-analytics/)                                                                              | The roadmap for Product Analytics at GitLab                       |
| [Acquisition, Conversion, Product Analytics Development Process](/handbook/engineering/development/growth/conversion-product-analytics/) | The development process for the Acquisition, Conversion, and Product Analytics groups         |
| [Product Analytics Guide](https://docs.gitlab.com/ee/development/product-analytics/index.html)                                                    | An overview of our collection tools                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Event Dictionary](https://docs.gitlab.com/ee/development/product_analytics/event_dictionary.html)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Product Performance Indicators Workflow](/handbook/product/performance-indicators#product-analytics-workflow)                                   | The workflow for putting product PIs and XMAUs in place   |
| [Data Team: Creating Charts](/handbook/business-ops/data-team/programs/data-for-product-managers/)                                | How to create your own dashboard                          |
| [Data Team: Data Warehouse](/handbook/business-ops/data-team/platform/#data-warehouse)                                            | An outline of where our product usage data is stored      |
| [Data Team: dbt Guide](/handbook/business-ops/data-team/platform/dbt-guide/)                                                      | How we transform raw data into a data structure that's ready for analysis |
| [Growth Product Direction](/direction/growth/)                                                                                    | The roadmap for Growth at GitLab                          |
| [Growth Product Process](/handbook/product/growth/)                                                                              | The product process for the Growth sub-department         |
| [Growth Sub-Department Development Process](/handbook/engineering/development/growth/).                                              | The development process for the Growth sub-department     |
| [Growth Sub-Department Performance Indicators Page](/handbook/engineering/development/growth/performance-indicators/)              | The performance indicators for the Growth sub-department  |
| [Growth UX Process](/handbook/engineering/ux/stage-group-ux-strategy/growth/)                                                        | The UX process for the Growth sub-department              |
| [Growth QE Process](/handbook/engineering/quality/growth-qe-team/)                                                                   | The QE process for the Growth sub-department              |

